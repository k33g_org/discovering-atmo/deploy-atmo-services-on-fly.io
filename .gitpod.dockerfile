FROM gitpod/workspace-full

RUN sudo apt-get update && \
    sudo apt-get install gettext -y

USER gitpod

RUN brew tap suborbital/subo && \
    brew install subo && \
    brew install exa && \
    brew install bat && \
    brew install httpie && \
    brew install hey && \
    brew install superfly/tap/flyctl

