#!/bin/bash
# build and push to GitLab registry
cd services
 
gitlab_handle="k33g" # use your own handle
docker login registry.gitlab.com -u ${gitlab_handle} -p ${GITLAB_TOKEN_ADMIN}

registry="registry.gitlab.com/k33g_org/discovering-atmo/deploy-atmo-services-on-fly.io"
app_name="atmo-demo"
tag="latest"

docker build -t ${app_name} . 
docker tag ${app_name} ${registry}/${app_name}:${tag}
docker push ${registry}/${app_name}:${tag}
