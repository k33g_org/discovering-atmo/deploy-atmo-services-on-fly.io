# Atmo Workspace Json Payload

## What is Atmo?

**[Atmo](https://github.com/suborbital/atmo)** is a project from [Suborbital](https://suborbital.dev/). 

To say it short, Atmo is a toolchain allowing writing and deploying wasm microservices smoothly.
With Atmo, you can write microservices in Rust, Swift, and AssemblyScript without worrying about complicated things, and you only have to care about your source code:

- No fight with wasm and string data type
- No complex toolchain to install
- Easy deployment with Docker (and then it becomes trivial to deploy wasm microservices on Kubernetes)
- ...

## About this project

This is a running demo to demonstrate how to serve a JSON REST API with Atmo.

This project contains 2 Runnables (or functions) written in Rust:

- `hello` (see `./services/hello/src/lib.rs`) that produces a JSON object
- `restaurants` (see `./services/restaurants/src/lib.rs`) that produces an Array of JSON objects

### 🤚 To begin, click on the **Gitpod** button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/k33g_org/discovering-atmo/deploy-atmo-services-on-fly.io)

## How to build the Runnables

```bash
cd services
subo build .
```

## Serve the Runnables

```bash
cd services
subo dev
```

## Call the Service

### Get a json object

```bash
http http://localhost:8080/hello
```

> Expected result:
```bash
Content-Length: 49
Content-Type: application/json; charset=utf-8
Date: Tue, 26 Oct 2021 13:26:24 GMT

{
    "author": "@k33g",
    "text": "👋 Hello World 🌏"
}
```

### Get a json array

```bash
http http://localhost:8080/restaurants
http https://atmo-demo.fly.dev/restaurants
```

> Expected result:
```bash
Content-Length: 309
Content-Type: application/json; charset=utf-8
Date: Tue, 26 Oct 2021 14:37:31 GMT

[
    {
        "address": "17 rue Alexandre Dumas, 75011 Paris",
        "name": "Le Ciel",
        "phone": "(33)664 441 416"
    },
    {
        "address": "87 rue de la Roquette, 75011 Paris",
        "name": "A La Renaissance",
        "phone": "(33)143 798 309"
    },
    {
        "address": "30 rue de la Folie Méricourt, 75011 Paris",
        "name": "La Cave de l'Insolite",
        "phone": "(33)153 360 833"
    }
]
```

## Embed the bundle in a container (and push to the GitLab registry)
> You can use of course any container registry

### Login to the registry

```bash 
gitlab_handle="k33g" # use your own handle
docker login registry.gitlab.com -u ${gitlab_handle} -p ${GITLAB_TOKEN_ADMIN}
```

> `GITLAB_TOKEN_ADMIN` is a personal access token (I'm storing it to the Gitpod variables)

### Build the image

```bash
# Build the application
cd services
subo build .

# Build container image
registry="registry.gitlab.com/k33g_org/discovering-atmo/deploy-atmo-services-on-fly.io"
app_name="atmo-demo"
tag="0.0.1"

docker build -t ${app_name} . 
docker tag ${app_name} ${registry}/${app_name}:${tag}
docker push ${registry}/${app_name}:${tag}
```

## Deployment on  Fly.io

### Prerequisites

#### Install the Fly.io CLI

See 👀 https://fly.io/docs/getting-started/installing-flyctl/

#### Create an account on Fly.io (if it's your first time with Fly.io)

```bash
flyctl auth signup
# you need a credit card
```

#### Create a Fly.io token

- Use https://web.fly.io/user/personal_access_tokens/new
- Copy the generated value
- If you work with Gitpod:
  - create an environment variable `FLY_ACCESS_TOKEN` (with the token as value) in your Gitpod account
- **🖐️ about GitLab CI**: create an environment variable `FLY_ACCESS_TOKEN` (with the token value) at the group level or project level 

### Deploy

#### Configuration

You need to create a `fly.toml` file at the root of the Atmo project `services` (it's the application configuration file: https://fly.io/docs/reference/configuration/)

```toml
kill_signal = "SIGINT"
kill_timeout = 5
processes = []

[experimental]
  allowed_public_ports = []
  auto_rollback = true

[[services]]
  http_checks = []
  internal_port = 8080
  processes = ["app"]
  protocol = "tcp"
  script_checks = []

  [services.concurrency]
    hard_limit = 25
    soft_limit = 20
    type = "connections"

  [[services.ports]]
    handlers = ["http"]
    port = 80

  [[services.ports]]
    handlers = ["tls", "http"]
    port = 443

  [[services.tcp_checks]]
    grace_period = "1s"
    interval = "15s"
    restart_limit = 0
    timeout = "2s"
```

#### Deployment

```bash
# Create the application, only at the first deplyment
flyctl apps create ${app_name} --json

# Deploy
flyctl deploy \
  --app ${app_name} \
  --image ${registry}/${app_name}:${tag} \
  --env ATMO_HTTP_PORT=8080 \
  --verbose --json

# Wait for a moment
echo "🌍 https://${app_name}.fly.dev/restaurants"
echo "🌍 https://${app_name}.fly.dev/hello"
```
