use suborbital::runnable::*;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct ResponseMessage {
    text: String,
    author: String,
    message: String
}

struct Hello{}

impl Runnable for Hello {
    fn run(&self, _: Vec<u8>) -> Result<Vec<u8>, RunErr> {
        let response_message = ResponseMessage { 
            text: String::from("👋 Hello World 🌏🌎🌏"), 
            author: String::from("🚀 @k33g 🐼"),
            message: String::from("🚧 this is a work in progress")
        };

        let response_string = serde_json::to_string(&response_message).unwrap();

        suborbital::resp::content_type("application/json; charset=utf-8");
    
        Ok(response_string.as_bytes().to_vec())
    }
}


// initialize the runner, do not edit below //
static RUNNABLE: &Hello = &Hello{};

#[no_mangle]
pub extern fn _start() {
    use_runnable(RUNNABLE);
}
