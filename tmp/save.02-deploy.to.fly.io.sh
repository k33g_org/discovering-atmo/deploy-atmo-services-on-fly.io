#!/bin/bash

# Build the application
cd services
subo build .

# Build container image
gitlab_handle="k33g" # use your own handle
docker login registry.gitlab.com -u ${gitlab_handle} -p ${GITLAB_TOKEN_ADMIN}

registry="registry.gitlab.com/k33g_org/discovering-atmo/deploy-atmo-services-on-fly.io"
app_name="atmo-demo"
tag="latest"

docker build -t ${app_name} . 
docker tag ${app_name} ${registry}/${app_name}:${tag}
docker push ${registry}/${app_name}:${tag}

FLY_APP_CREATED=false
flyctl apps create ${app_name} --json || FLY_APP_CREATED=true

flyctl deploy \
  --app ${app_name} \
  --image ${registry}/${app_name}:${tag} \
  --env ATMO_HTTP_PORT=8080 \
  --verbose --json

echo "🌍 https://${app_name}.fly.dev/restaurants"
echo "🌍 https://${app_name}.fly.dev/hello"